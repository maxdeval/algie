# Train your algorithm skills with Javascript

TDD based, beginner friendly, zero installation process

## Usage

Download or clone this Github repository.

Open index.html to see the specs.

Modify src/Algie, and reload index.html, until all tests are green

## How to teach algorithm to your student

 - Learn him/her to google around if the problem has already been solved somewhere
 - If not, learn him/her to break the problem into a suit of smaller problems
 - Learn him/her to google around each of the smaller problem, to see if this smaller problem has already been solved somewhere
 - If not, learn him/her how to use Stackoverflow.
 - Learn him/her how to find and use other channels than Stackoverflow.
 - Learn him/her to rely a lot on librairies. In JavaScript, Lodash is very efficient to solve algorithms problems.
 - Learn him/her a more functional approach, which is more robust and efficient.
 - Learn him/her not to be afraid of huge chunk of code or API response. This is how it happens in the real world, and "huge" doesn't necesseraly mean "more difficult".
 - Learn him/her to commit very frequently, so that you, as teacher, can follow how the progression was made.
 - Learn him/her to use TDD with at least 2 examples on the "nominal path".
 - Learn him/her to use TDD with weird cases.
 - Learn him/her to think about incomplete spec.
 - Learn him/her to think about side-effects.
 - Learn him/her how to debug efficiently.
 - Learn him/her to train every day.
 - Learn him/her not to do too difficult task when training, because this won't help.
 - Learn him/her not to do too easy task when training, because he/she won't learn anything new.
 - Discuss with him/her about the solution and how to improve it.
 - Make him/her redo an exercice that was already solved a few weeks ago.

## Motivation

There are already tons of website about algorithm exercising.

 - it is open-sourced : you can add exercices as you wish,
 - most problems are extracted from real-world projects,
 - even if problems can only be solved in JavaScript, they are not JavaScript related. You can use the spec to train in another language.
